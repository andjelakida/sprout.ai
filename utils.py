import ast
import csv

from typing import Dict, List

def get_employees_info(path: str) -> Dict:
    with open(f"{path}/employees.txt", "r") as employees_file:
        employees_data = employees_file.read()

        # ast.literal_eval is safer than eval 
        # because it restricts evaluation to a subset of valid python datatypes
        employees_info = ast.literal_eval(employees_data)
        return employees_info

def get_tasks(path: str) -> List:
    with open(f"{path}/tasks.csv", "r") as task_file:
        tasks = [
            { k: v for k, v in row.items() }
            for row in csv.DictReader(task_file, skipinitialspace=True)
        ]
        return tasks