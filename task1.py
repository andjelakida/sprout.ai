from typing import Dict, List, Set

from utils import get_employees_info, get_tasks

def assign_tasks_1(file_path: str = "input", use_types: bool = False) -> Dict:
    employees_info: Dict = get_employees_info(path=file_path)
    tasks: List = get_tasks(path=file_path)

    employees_by_task_type: Dict[str, List] = group_employees_by_task_type(
        employees_info=employees_info,
        use_types=use_types
    )
    task_types: List = list(employees_by_task_type.keys())

    # first, for each task type, add pre-assigned tasks to the assignments dict
    work_assignments_by_type: Dict[str, Dict] = process_preassigned_tasks(
        tasks=tasks, 
        employees_info=employees_info,
        task_types=task_types,
        use_types=use_types
    )

    # then, for each task type, using a modified round robin approach, assign unasigned task to employees
    # modified round robin approach:   

    process_unassigned_tasks(
        tasks=tasks,
        work_assignments_by_type=work_assignments_by_type,
        employees_info=employees_info,
        employees_by_task_type=employees_by_task_type,
        task_types=task_types,
        use_types=use_types
    )
            
    print_results(work_assignments_by_type=work_assignments_by_type)

    return work_assignments_by_type

def group_employees_by_task_type(employees_info: Dict, use_types: bool) -> Dict:
    employees_by_task_type: Dict[str, List] = {}
    for employee_name, employee_type in employees_info.items():
        # for task 1, we don't care about task_types
        task_type = employee_type if use_types else "all"
        employees_by_task_type.setdefault(task_type, []).append(employee_name)
    
    return employees_by_task_type

def process_preassigned_tasks(tasks: List, employees_info: Dict, task_types: Set, use_types: bool):
    work_assignments_by_type: Dict[str, Dict] = {tt: {} for tt in task_types}

    for task in tasks:
        employee = task["user"]
        task_id = task["task_id"]
        task_type = task["type"] if use_types else "all"

        if employee != "None":
            if not use_types or (use_types and task_type == employees_info[employee]):
                work_assignments_by_type[task_type].setdefault(employee, []).append(task_id)

    return work_assignments_by_type

def find_minimal_amount_of_tasks(work_assignments: Dict) -> int:
    return min(map(len, work_assignments.values()))

def process_unassigned_tasks(
    tasks: List,
    work_assignments_by_type: Dict,
    employees_info: Dict, 
    employees_by_task_type: Dict,
    task_types: List, 
    use_types: bool
) -> None:
    #  - starting iteration index is the minimal amount of tasks any employee has been pre-assigned with
    #    (this means that all employees have been pre-assigned with at least that amount of tasks)
    #  - assign tasks by iterating through the list of employees, 
    #    only assigning if the employee has minimal amount of tasks
    #  - if the employee has more than the minimal amount of tasks, iterate through employees
    #    until an employee that satisfies the condition is found(this will always happen)
    #  - once the iteration through the employees list is finished, 
    #    it means that all employees have minimal amount + 1 tasks
    #    so the minimal amount must be incremented, and iteration through employees started again   
    e_indices: Dict[str, int] = { tt: 0 for tt in task_types }
    rr_indices: Dict[str, int] = {
        tt: find_minimal_amount_of_tasks(work_assignments_by_type[tt]) 
        for tt in task_types
    }

    for task in tasks:
        employee = task["user"]
        task_type = task["type"] if use_types else "all"
        task_id = task["task_id"]

        if employee == "None" or (use_types and employees_info[employee] != task_type):
            work_assignments = work_assignments_by_type[task_type]
            e_idx = e_indices[task_type]
            rr_idx = rr_indices[task_type]
            employees = employees_by_task_type[task_type]

            while len(work_assignments[employees[e_idx]]) > rr_idx:
                e_idx += 1

            work_assignments[employees[e_idx]].append(task_id)
            e_idx += 1

            if e_idx == len(employees):
                e_idx = 0
                rr_idx += 1

            e_indices[task_type] = e_idx
            rr_indices[task_type] = rr_idx

def print_results(work_assignments_by_type: Dict) -> None:
    for _, vals in work_assignments_by_type.items():
        for name, tasks in vals.items():
            print(name, len(tasks))


if __name__ == '__main__':
    assign_tasks_1()
