import ast
import csv
from dataclasses import dataclass
from typing import Dict, List

import ast
import re
import os
import csv
import sys
from datetime import datetime

from utils import get_employees_info, get_tasks

DEFAULT_PRODUCTIVITY_FACTOR = 0.7

@dataclass(frozen=False)
class GroupInfo:
    type: str
    employees: List[str]
    assignments: Dict
    curr_idx: str
    rr_idx: str

def assign_tasks_3(file_path: str = "input") -> Dict:
    employees_info: Dict = get_employees_info(path=file_path)
    tasks: List = get_tasks(path=file_path)

    tasks = sort_tasks(tasks)
    employee_stats = calculate_user_stats(path=file_path)

    task_assignments: Dict[str, List] = assign_tasks(
        tasks=tasks,
        employees_info=employees_info,
        employee_stats=employee_stats
    )

    print_results(task_assignments=task_assignments)

    return task_assignments


def is_highly_productive(employee, employee_stats) -> bool:
    factor = float(sys.argv[1]) if len(sys.argv) > 1 else DEFAULT_PRODUCTIVITY_FACTOR
    average_productivity_rate = sum(employee_stats.values()) / len(employee_stats.values())
    return employee_stats[employee] < average_productivity_rate * factor

def sort_tasks(tasks: List) -> List:
    return sorted(
        tasks, key = lambda d: datetime.strptime(d["version"], "%Y-%m-%d").date()
    )

def calculate_user_stats(path: str) -> Dict:
    in_progress = {}
    user_stats = {}

    csv_files = [f for f in os.listdir(path) if re.match(r'[0-9]{8}.*.csv', f)]

    for csv_file in sorted(csv_files):
        todays_date = datetime.strptime(csv_file[:8], "%Y%m%d").date()

        with open(f"{path}/{csv_file}", "r") as day_file:
            todays_tasks = set()

            for row in csv.DictReader(day_file, skipinitialspace=True):
                user = row["user"]
                task_id = row["task_id"]

                todays_tasks.add(task_id)
                if task_id not in in_progress:
                    in_progress[task_id] = (todays_date, user)

            finished_tasks = set(in_progress.keys()) - todays_tasks
            for ft in finished_tasks:
                start_date, user = in_progress[ft]
                user_stats.setdefault(user, []).append((todays_date-start_date).days)
                del in_progress[ft]

    return {
        employee: sum(task_lenghts)/len(task_lenghts)
        for employee, task_lenghts in user_stats.items()
    }

def assign_tasks(tasks: List, employees_info: Dict, employee_stats: Dict) -> Dict:
    employees: List[str] = list(employees_info.keys())
    work_amount_per_employee: Dict[str, int] = {e: 0 for e in employees}
    task_assignments: Dict[str, List] = {e: [] for e in employees}

    # process tasks:
    #    - each employee starts with 0 tasks
    #    - tasks are assigned to the employees who can finish them in the fastest time
    #         * this means this employee needs to finish its tasks + the current one faster than any other employee
    for task in tasks:
        min_work: float = float("inf")
        employee: str = None

        for e, e_type in employees_info.items():
            if is_highly_productive(e, employee_stats) or e_type == task["type"]:
                if work_amount_per_employee[e] + employee_stats[e] < min_work:
                    min_work = work_amount_per_employee[e] + employee_stats[e]
                    employee = e
    
        work_amount_per_employee[employee] += employee_stats[employee]
        task_assignments[employee].append(task["task_id"])

    return task_assignments

def print_results(task_assignments: Dict) -> None:
    for name, tasks in task_assignments.items():
        print(name, len(tasks))

assign_tasks_3()

                

                

                    

