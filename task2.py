from typing import Dict

from task1 import assign_tasks_1

def assign_tasks_2() -> Dict:
    return assign_tasks_1(use_types=True)

assign_tasks_2()